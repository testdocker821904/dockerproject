const express = require("express");
const mysql = require("mysql2");
const cors = require("cors");
const bodyparser = require("body-parser");

const app = express();

app.use(bodyparser.json());

var mysqlConneciton = mysql.createConnection({
    host:"localhost",//host.docker.internal
    port: 3306,
    user: 'root',
    password: 'manager',
    database: 'sdmexam'
});

mysqlConneciton.connect((err)=>{
    if(!err){
        console.log("Connection to database succesfull");
    }else{
        console.log(err);
    };
});

app.get("/", (req,res)=>{
    res.send("Hello World");
});
//readall operation
app.get("/read", (req,res)=>{
    mysqlConneciton.query("select * from students_tb",[req.params.id],(err,result)=>{
        if(!err){
            res.send(result);
        }else{
            console.log(err);
        }
    });
});

//read operation
app.get("/read/:id", (req,res)=>{
    mysqlConneciton.query("select * from students_tb where s_name =?",[req.params.id],(err,result)=>{
        if(!err){
            res.send(result);
        }else{
            console.log(err);
        }
    });
});

//insert operation
app.post("/read", (req,res)=>{
    var values = req.body;
    mysqlConneciton.query("insert into students_tb values(?,?,?,?,?,?,?)",[values.student_id,values.s_name,values.password,values.course,values.passing_year,values.prn_no,values.dob],(err,result)=>{
        if(!err){
            res.send("success");
        }else{
            console.log(err);
        }
    });
});

//delete operation
app.delete("/read/:id", (req,res)=>{
    mysqlConneciton.query("delete from students_tb where dob =?",[req.params.id],(err,result)=>{
        if(!err){
            res.send("success");
        }else{
            console.log(err);
        }
    });
});

//update operation
app.put("/read", (req,res)=>{
    var values = req.body;
    mysqlConneciton.query("update students_tb set course=?,prn_no=? where student_id=?",[values.course,values.prn_no,values.student_id],(err,result)=>{
        if(!err){
            res.send("success");
        }else{
            console.log(err);
        }
    });
});

app.listen(3000,()=>{
    console.log("express listening at 3000");
})
















